package org.example.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
@PropertySource(value = "classpath:/value.properties")
public class WebConfig {

    @Autowired
    public Environment environment;

    @Bean
    public DataSource initDS(){
        DriverManagerDataSource source = new DriverManagerDataSource();
        source.setDriverClassName(environment.getProperty("jdbc.driver"));
        source.setUrl(environment.getProperty("jdbc.url"));
        source.setUsername(environment.getProperty("jdbc.username"));
        source.setPassword(environment.getProperty("jdbc.password"));

        return source;
    }

    @Bean
    public JdbcTemplate intJdbcTemp(){
        return intJdbcTemp();
    }

    @Bean
    public DataSourceTransactionManager initTM(){
        return initTM();
    }
}
